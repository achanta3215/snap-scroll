import React, { useEffect, useRef } from 'react';

interface IProps {
    url: string;
    isActive: boolean;
    key: string;
}

const UserVideo = ({ key, url, isActive }: IProps) => {
  const extraProps = isActive ? { 'data-is-active': true }: {};
  const videoRef = useRef<HTMLVideoElement>(null);
  useEffect(() => {
    if (videoRef.current && isActive) {
      videoRef.current.play();
    }
    return () => {
      videoRef.current.pause();
    }

  }, [isActive, videoRef.current]);
  return (
      <video muted ref={videoRef} src={url} className='video-post' {...extraProps} />
  );
};

export default UserVideo;