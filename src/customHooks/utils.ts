import { useCallback, useMemo } from "react";

export const useUtils = () => {
    const enableScroll = useCallback(() => { 
        window.onscroll = function() {}; 
    }, []);
    const disableScroll = useCallback(() => {
        const scrollTop = window.pageYOffset || document.documentElement.scrollTop; 
        const scrollLeft = window.pageXOffset || document.documentElement.scrollLeft;
        
            // if any scroll is attempted, set this to the previous value 
        window.onscroll = function() { 
            window.scrollTo(scrollLeft, scrollTop); 
        };
    }, []);
    const debounce = useCallback((func: any, wait = 100) => {
        let timeout: any = null;
        // return function(...args: any) {
        //     // @ts-ignore
        //     console.log('ScrollEvent triggered ' + window.isCustomScroll + window.scrollY)
        //     // // @ts-ignore
        //     // if (window.isCustomScroll) {
        //     //     func.apply(this, args);
        //     // }
        //     if (timeout !== null) return;
        //     if (timeout === null) {
        //         func.apply(this, args);
        //     }
        //   timeout = setTimeout(() => {
        //     clearTimeout(timeout);
        //     timeout = null;
        //   }, wait);
        // };
        return function(...args: any) {
            if (window.isScrollTriggered) {
                func.apply(this, args);
            }
            clearTimeout(timeout);
            timeout = setTimeout(() => {
                func.apply(this, args);
            }, wait);
        };
      }, []);
    return {
        enableScroll, disableScroll,debounce,
    };
};
