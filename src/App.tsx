import React, { useCallback, useEffect, useMemo, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import UserVideo from './UserVideo';
import videoPosts from './data.json';
import PaginatedList from './PaginatedList';
import { useUtils } from './customHooks/utils';

export type VideoPosts = typeof videoPosts;

function App() {
  const [currentIndex1, setCurrentIndex] = useState(0);
  const { disableScroll, enableScroll, debounce } = useUtils();
  const getVideoPostElements = useCallback(
    () => Array.from(document.getElementsByTagName('video')),
  []);
  const getVideoPostDestinationTarget = useCallback(
    (index) => getVideoPostElements()[index].getBoundingClientRect().y,
    [],
  );
  const updateIndex = useCallback((e) => {
    const activeIndex = getVideoPostElements()
      .findIndex(videoPost => videoPost.getAttribute('data-is-active'));
    const nextIndexToScrollTo = activeIndex + 1;
    console.log('window Y' + window.scrollY);
    // Start scrolling to target destination
    if (!window.isScrollTriggered || window.previousScroll === window.scrollY) {
      const scrollToElement = document.getElementsByTagName('video')[nextIndexToScrollTo];
      console.log('Scroll triggered to index ' + nextIndexToScrollTo
        + 'to Element' + scrollToElement);
        // window.onscroll = function() {}; 

      document.getElementsByTagName('video')[nextIndexToScrollTo]
      // document.getElementsByTagName()
      // .scrollTo({ behavior: 'smooth'})
        .scrollIntoView({ behavior: 'smooth' });

      window.isScrollTriggered = true;
    }
    // Compute target destination as the next element's position
    if (window.scrollY < getVideoPostDestinationTarget(nextIndexToScrollTo)
      && window.scrollY > getVideoPostDestinationTarget(activeIndex)) {
      window.targetDestination = getVideoPostDestinationTarget(nextIndexToScrollTo);
    }
    // Reached target desintation
    console.log('target' + getVideoPostDestinationTarget(nextIndexToScrollTo));
    if (getVideoPostDestinationTarget(nextIndexToScrollTo) < 1) {
      setTimeout(() => {
        setCurrentIndex(nextIndexToScrollTo);
        window.isScrollTriggered = false;
      }, 500);
    }
    window.previousScroll = window.scrollY;
  }, []);
  useEffect(() => {
    // disableScroll();
    const debounced = debounce(updateIndex, 150);
    window.addEventListener('scroll', debounced);
    // window.addEventListener('scroll', updateIndex);


    window.addEventListener('mouseup', () => { console.log('mouseup');});
  }, []);
  return (
    <div className='flex-root'>
      <div className='left-container' />
      <div className='posts-column'>
        <PaginatedList
          pageCount={6}
          render={({ currentIndex }) => (
            videoPosts.map((videoPost, index) => (
              <UserVideo
                key={videoPost.id}
                url={videoPost.video.smallUrl}
                isActive={index === currentIndex1}
              />
            ))
          )}
        />
      </div>
      <div className='right-container'></div>
    </div>
  );
}

export default App;
