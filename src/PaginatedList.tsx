import React from 'react';
import UserVideo from './UserVideo';

interface RestProps {
    currentIndex: number;
}

interface IProps {
    pageCount: number;
    render: (a: RestProps) => any; 
}

const PaginatedList = (props: IProps) => {
    const { render, ...restProps } = props;
    return render({ ...restProps, currentIndex: 0 });
};



export default PaginatedList;