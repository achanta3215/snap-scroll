export {}
declare global {
    interface Window {
        targetDestination: number;
        previousScroll: number;
        isScrollTriggered: boolean;
    }
}

// window.MyNamespace = window.MyNamespace || {};
